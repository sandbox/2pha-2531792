(function ($) {
  Drupal.behaviors.location_cookie_admin = {
    attach:function(context, settings){
      var checkcontainer = $('<div class="form-item"></div>');
      var checkbutton = $('<input id="checkbtn" type="button" class="form-submit" value="Check service"/>');
      checkbutton.click(function(){
        checkbutton.after('<div id="check-progress" class="ajax-progress"><div class="throbber"></div></div>');
        
        $.ajax({
          url: $('#edit-location-cookie-service').val(),
          success: function(data){
            $('#check-progress').remove();
            $('#checkresultscontainer').remove();
            console.log(data);
            var cookienames = '';
            var count = 0;
            for(var i in data){
              cookienames += 'LOCATIONCOOKIE.' + i + ' = ' + data[i] +'\n';
              count++;
            }
            var textarea = $('<div id="checkresultscontainer" class="form-item"></div>')
                            .append('<label>Raw Data</label><textarea id="checkrawresults" style="width: 100%">'+JSON.stringify(data)+'</textarea>')
                            .append('<label>Cookies that will be set</label><textarea id="checkcookienames" rows="'+count+'" style="width: 100%">'+cookienames+'</textarea>');
            
            $('#checkbtn').after(textarea);
          }
        });
      });
      checkcontainer.append(checkbutton);
      $('#edit-actions', context).before(checkcontainer);
    }
  };
})(jQuery)