(function ($) {
  Drupal.behaviors.location_cookie = {
    attach:function(context, settings){
      if ((typeof settings.location_cookie.service !== 'undefined' && settings.location_cookie.service && !locationCookieRead('LOCATIONCOOKIE_ip')) || settings.location_cookie.force) {
        locationCookieSet(settings);
      }
    }
  };
  
  function locationCookieSet(settings) {
    $.ajax({
      url: settings.location_cookie.service,
      success: function(data){
        for(var i in data){
          locationCookieCreate('LOCATIONCOOKIE_'+i, data[i], settings.location_cookie.life, settings.basePath);
        }
      }
    });
  }
  
  function locationCookieCreate(name,value,days,path) {
    days = parseInt(days);
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path="+path;
  }
  
  function locationCookieRead(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }

  function locationCookieErase(name) {
    createCookie(name,"",-1);
  }
  
})(jQuery)